package com.programik.lukasz.grajkowik;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.media.MediaPlayer;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    // guziki
    Button guzik11;
    Button guzik12;
    Button guzikStop;
    Button guzik21;
    Button guzik22;
    Button guzik23;
    Button guzik24;
    Button guzik31;
    Button guzik32;
    Button guzik33;
    Button guzik34;

    // muzyczki i grania
    MediaPlayer guzik11mp3;
    MediaPlayer guzik12mp3;
    MediaPlayer guzik21mp3;
    MediaPlayer guzik22mp3;
    MediaPlayer guzik23mp3;
    MediaPlayer guzik24mp3;
    MediaPlayer guzik31mp3;
    MediaPlayer guzik32mp3;
    MediaPlayer guzik33mp3;
    MediaPlayer guzik34mp3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // guziki
        guzik11 = (Button) findViewById(R.id.guzik11);
        guzik11.setOnClickListener(this);

        guzik12 = (Button) findViewById(R.id.guzik12);
        guzik12.setOnClickListener(this);

        guzikStop = (Button) findViewById(R.id.guzikStop);
        guzikStop.setOnClickListener(this);

        guzik21 = (Button) findViewById(R.id.guzik21);
        guzik21.setOnClickListener(this);

        guzik22 = (Button) findViewById(R.id.guzik22);
        guzik22.setOnClickListener(this);

        guzik23 = (Button) findViewById(R.id.guzik23);
        guzik23.setOnClickListener(this);

        guzik24 = (Button) findViewById(R.id.guzik24);
        guzik24.setOnClickListener(this);

        guzik31 = (Button) findViewById(R.id.guzik31);
        guzik31.setOnClickListener(this);

        guzik32 = (Button) findViewById(R.id.guzik32);
        guzik32.setOnClickListener(this);

        guzik33 = (Button) findViewById(R.id.guzik33);
        guzik33.setOnClickListener(this);

        guzik34 = (Button) findViewById(R.id.guzik34);
        guzik34.setOnClickListener(this);

        // muzyczki i grania
        guzik11mp3 = MediaPlayer.create(this, R.raw.intro);
        guzik12mp3 = MediaPlayer.create(this, R.raw.koniec);
        guzik21mp3 = MediaPlayer.create(this, R.raw.klawiatura);
        guzik22mp3 = MediaPlayer.create(this, R.raw.radio);
        guzik23mp3 = MediaPlayer.create(this, R.raw.dyskietka);
        guzik24mp3 = MediaPlayer.create(this, R.raw.modem);
        guzik31mp3 = MediaPlayer.create(this, R.raw.wtyczka);
        guzik32mp3 = MediaPlayer.create(this, R.raw.kaseta);
        guzik33mp3 = MediaPlayer.create(this, R.raw.laser);
        guzik34mp3 = MediaPlayer.create(this, R.raw.cdrom);
    }

    @Override
    public void onClick(View v) {
        int callerId = v.getId();
        switch(callerId)
        {
            case R.id.guzik11:
                guzik11mp3.start();
                break;
            case R.id.guzik12:
                guzik12mp3.start();
                break;
            case R.id.guzik21:
                guzik21mp3.start();
                break;
            case R.id.guzik22:
                guzik22mp3.start();
                break;
            case R.id.guzik23:
                guzik23mp3.start();
                break;
            case R.id.guzik24:
                guzik24mp3.start();
                break;
            case R.id.guzik31:
                guzik31mp3.start();
                break;
            case R.id.guzik32:
                guzik32mp3.start();
                break;
            case R.id.guzik33:
                guzik33mp3.start();
                break;
            case R.id.guzik34:
                guzik34mp3.start();
                break;

            case R.id.guzikStop:
                // guzik STOP pauzuje wszystkie grane dźwięki oraz cofa je do poczatku
                if (guzik11mp3.isPlaying()) {
                    guzik11mp3.pause();
                    guzik11mp3.seekTo(0);
                }
                if (guzik12mp3.isPlaying()) {
                    guzik12mp3.pause();
                    guzik12mp3.seekTo(0);
                }
                if (guzik21mp3.isPlaying()) {
                    guzik21mp3.pause();
                    guzik21mp3.seekTo(0);
                }
                if (guzik22mp3.isPlaying()) {
                    guzik22mp3.pause();
                    guzik22mp3.seekTo(0);
                }
                if (guzik23mp3.isPlaying()) {
                    guzik23mp3.pause();
                    guzik23mp3.seekTo(0);
                }
                if (guzik24mp3.isPlaying()) {
                    guzik24mp3.pause();
                    guzik24mp3.seekTo(0);
                }
                if (guzik31mp3.isPlaying()) {
                    guzik31mp3.pause();
                    guzik31mp3.seekTo(0);
                }
                if (guzik32mp3.isPlaying()) {
                    guzik32mp3.pause();
                    guzik32mp3.seekTo(0);
                }
                if (guzik33mp3.isPlaying()) {
                    guzik33mp3.pause();
                    guzik33mp3.seekTo(0);
                }
                if (guzik34mp3.isPlaying()) {
                    guzik34mp3.pause();
                    guzik34mp3.seekTo(0);
                }
                break;
        }
    }
}
